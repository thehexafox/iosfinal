//
//  TblCell.swift
//  Time Clock
//
//  Created by Jeffrey Farnworth on 4/20/16.
//  Copyright © 2016 Jeffrey Farnworth. All rights reserved.
//

import UIKit

class TblCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

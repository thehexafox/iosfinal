//
//  ViewController.swift
//  Time Clock
//
//  Created by Jeffrey Farnworth on 4/8/16.
//  Copyright © 2016 Jeffrey Farnworth. All rights reserved.
//

import UIKit
//import RealmSwift

class ViewController: UIViewController, UITableViewDataSource {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //register custom cell
        var nib = UINib(nibName: "vwTblCell", bundle: nil)
        timeListTableView.registerNib(nib, forCellReuseIdentifier: "cell")
        Categors.insert(Category(), atIndex: 0)
        //Initial Work category
        Categors[0].categoryName = "Work"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var isTiming = false //makes Start/Stop buttons exclusive

    @IBOutlet weak var timeListTableView: UITableView!
    @IBOutlet weak var displayTimeLabel: UILabel! //time label
    @IBOutlet weak var totalLabel: UILabel! //total label
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var addCategoryBox: UITextField!
    
    //List of categories
    var categories = ["Work"]
    var categoryList = [String]()
    
    //UIPicker current selection
    var currentSelection = "Work"
    
    //array of Categories
    var Categors = [Category]()
    
    //Add Button
    @IBAction func addCategoryButton(sender: AnyObject) {
        categories.insert(addCategoryBox.text!, atIndex: 0)
        
        Categors.insert(Category(), atIndex: 0)
        Categors[0].categoryName = addCategoryBox.text!
        addCategoryBox.text = ""
        categoryPicker.reloadAllComponents()
        timeListTableView.reloadData()
        print(Categors[0].categoryName)

//        This is what I'd do to make the information persist if Realm worked with my program...
//        Since the categories save their times via array, it doesn't currently work.
//        I didn't realize that was an issue until my app was well underway and I needed a working copy for the demo.
//        let realm = try! Realm()
//        
//        try! realm.write(){
//            realm.add(Categors[0])
//        }
    }
    
    @IBAction func start(sender: AnyObject) {
        if !isTiming{
            if !timer.valid {
                let aSelector : Selector = "updateTime"
                timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: aSelector, userInfo: nil, repeats: true)
                Categors[categoryPicker.selectedRowInComponent(0)].startTime = NSDate.timeIntervalSinceReferenceDate()
            }
        }
        isTiming = true
    }
    
    //Stop Button
    @IBAction func stop(sender: AnyObject) {
        timeListTableView.reloadData()
        if isTiming{
            timer.invalidate()
            var endTime = NSDate.timeIntervalSinceReferenceDate()
            
            //Find the difference between current time and start time.
            //var totalTime: NSTimeInterval = endTime - startTime
            
            Categors[categoryPicker.selectedRowInComponent(0)].totalTimeList.insert(endTime - Categors[categoryPicker.selectedRowInComponent(0)].startTime, atIndex: 0)
            Categors[categoryPicker.selectedRowInComponent(0)].thisInstanceTime = endTime - Categors[categoryPicker.selectedRowInComponent(0)].startTime
            Categors[categoryPicker.selectedRowInComponent(0)].totalTotalTime += endTime - Categors[categoryPicker.selectedRowInComponent(0)].startTime
            var totalTime = Categors[categoryPicker.selectedRowInComponent(0)].totalTotalTime
            
            //calculate elapsed time in hours
            let tHours = Int(totalTime / 3600.0)
            totalTime -= (NSTimeInterval(tHours) * 3600)
            
            //calculate this instance's hours
            let instanceHours = Int(Categors[categoryPicker.selectedRowInComponent(0)].thisInstanceTime / 3600.0)
            Categors[categoryPicker.selectedRowInComponent(0)].thisInstanceTime -= (NSTimeInterval(instanceHours) * 3600)
            
            //calculate elapsed time in minutes.
            let tMinutes = Int(totalTime / 60.0)
            totalTime -= (NSTimeInterval(tMinutes) * 60)
            
            //calculate this instance's minutes
            let instanceMinutes = Int(Categors[categoryPicker.selectedRowInComponent(0)].thisInstanceTime / 60.0)
            Categors[categoryPicker.selectedRowInComponent(0)].thisInstanceTime -= (NSTimeInterval(instanceMinutes) * 60)
            
            //calculate elapsed time in seconds.
            let tSeconds = Int(totalTime)
            totalTime -= NSTimeInterval(tSeconds)
            
            //calculate this instance's seconds
            let instanceSeconds = Int(Categors[categoryPicker.selectedRowInComponent(0)].thisInstanceTime)
            Categors[categoryPicker.selectedRowInComponent(0)].thisInstanceTime -= NSTimeInterval(instanceSeconds)
            
            //find out the fraction of milliseconds to be displayed.
            let tFraction = Int(totalTime * 100)
            
            //find out this instance's milliseconds
            let instanceFraction = Int(Categors[categoryPicker.selectedRowInComponent(0)].thisInstanceTime * 100)
            
            
            totalLabel.text = "\(String(format: "%02d", tHours)):\(String(format: "%02d", tMinutes)):\(String(format: "%02d", tSeconds)):\(String(format: "%02d", tFraction))"
            Categors[categoryPicker.selectedRowInComponent(0)].totalTimeListString.insert("\(String(format: "%02d", instanceHours)):\(String(format: "%02d", instanceMinutes)):\(String(format: "%02d", instanceSeconds)):\(String(format: "%02d", instanceFraction))", atIndex: 0)
       
            Categors[categoryPicker.selectedRowInComponent(0)].timeDates.insert(NSDate(), atIndex: 0)

            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Day , .Month , .Year], fromDate: date)
            let year =  components.year
            let month = components.month
            let day = components.day
            Categors[categoryPicker.selectedRowInComponent(0)].timeDatesString.insert("\(String(month))-\(String(day))-\(String(year))" ,atIndex: 0)
            categoryList.insert(categories[categoryPicker.selectedRowInComponent(0)], atIndex: 0)
        }
        
        isTiming = false
        timeListTableView.reloadData()
        //timer == nil
    }
    
    func updateTime() {
        var currentTime = NSDate.timeIntervalSinceReferenceDate()
        
        //Find the difference between current time and start time.
        var elapsedTime: NSTimeInterval = currentTime - Categors[categoryPicker.selectedRowInComponent(0)].startTime
        
        //calculate elapsed time in hours
        let hours = Int(elapsedTime / 3600.0)
        elapsedTime -= (NSTimeInterval(hours) * 3600)
        
        //calculate elapsed time in minutes.
        let minutes = Int(elapsedTime / 60.0)
        elapsedTime -= (NSTimeInterval(minutes) * 60)
        
        //calculate elapsed time in seconds.
        let seconds = Int(elapsedTime)
        elapsedTime -= NSTimeInterval(seconds)
        
        //find out the fraction of milliseconds to be displayed.
        let fraction = Int(elapsedTime * 100)
        
        //add the leading zero for minutes, seconds and millseconds and store them as string constants
        let strHours = String(format: "%02d", hours)
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        let strFraction = String(format: "%02d", fraction)
        
        //concatenate minutes, seconds and milliseconds as assign it to the UILabel
        displayTimeLabel.text = "\(strHours):\(strMinutes):\(strSeconds):\(strFraction)"
    }
    
    var timer = NSTimer()
    
    //how many sections are in the table
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //return int, how many rows
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Categors[categoryPicker.selectedRowInComponent(0)].totalTimeList.count
    }
    
    //what are the contents of each cell
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:TblCell = timeListTableView.dequeueReusableCellWithIdentifier("cell") as! TblCell
//        cell.timeLabel.text = ""
//        cell.dateLabel.text = ""
//        cell.categoryLabel.text = ""
        if(Categors[categoryPicker.selectedRowInComponent(0)].categoryName == categoryList[indexPath.row]){
            var timeListItem = Categors[categoryPicker.selectedRowInComponent(0)].totalTimeListString[indexPath.row]
            var timeDateDate = Categors[categoryPicker.selectedRowInComponent(0)].timeDatesString[indexPath.row]
            var cats = categoryList[indexPath.row]
            
            cell.timeLabel.font = UIFont.systemFontOfSize(17.0)
            cell.timeLabel.text = String(timeListItem)
            cell.dateLabel.text = timeDateDate
            cell.categoryLabel.text = cats
            
            if ((indexPath.row + 1 < Categors[categoryPicker.selectedRowInComponent(0)].timeDatesString.count) && (Categors[categoryPicker.selectedRowInComponent(0)].timeDatesString[indexPath.row] != Categors[categoryPicker.selectedRowInComponent(0)].timeDatesString[indexPath.row + 1])){ //total dates
                var previousDate = Categors[categoryPicker.selectedRowInComponent(0)].timeDatesString[indexPath.row + 1]
                var i = 1
                var ttl = 0.0
                print("Previous Date: " + previousDate)
                print("i: " + String(i))
                print("timeDatesString: " + Categors[categoryPicker.selectedRowInComponent(0)].timeDatesString[indexPath.row + 1])
                while ((indexPath.row + i < Categors[categoryPicker.selectedRowInComponent(0)].timeDatesString.count) && (previousDate == Categors[categoryPicker.selectedRowInComponent(0)].timeDatesString[indexPath.row + i])){
                    ttl += Categors[categoryPicker.selectedRowInComponent(0)].totalTimeList[indexPath.row + i]
                    print("During while loop")
                    i++
                }
                
                //calculate elapsed time in hours
                let hours = Int(ttl / 3600.0)
                ttl -= (NSTimeInterval(hours) * 3600)
                
                //calculate elapsed time in minutes.
                let minutes = Int(ttl / 60.0)
                ttl -= (NSTimeInterval(minutes) * 60)
                
                //calculate elapsed time in seconds.
                let seconds = Int(ttl)
                ttl -= NSTimeInterval(seconds)
                
                //find out the fraction of milliseconds to be displayed.
                let fraction = Int(ttl * 100)
                
                //add the leading zero for minutes, seconds and millseconds and store them as string constants
                let strHours = String(format: "%02d", hours)
                let strMinutes = String(format: "%02d", minutes)
                let strSeconds = String(format: "%02d", seconds)
                let strFraction = String(format: "%02d", fraction)
                
                cell.timeLabel.font = UIFont.boldSystemFontOfSize(20.0)
                cell.timeLabel.text = "Total for " + previousDate + ": " + "\(strHours):\(strMinutes):\(strSeconds):\(strFraction)"
                cell.dateLabel.text = ""
                cell.categoryLabel.text = ""
                
                return cell
            }
            return cell
        }
        return cell
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Categors.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return Categors[row].categoryName
    }
    
    var pickerValue = 0
    
    func pickerView(pickerView: UIPickerView!, didSelectRow row: Int, inComponent component: Int)
    {
        pickerValue++
        print("Changed Picker:" + String(pickerValue))
        currentSelection = Categors[categoryPicker.selectedRowInComponent(0)].categoryName
        print(currentSelection + " is currently selected")
        print(Categors[categoryPicker.selectedRowInComponent(0)].categoryName)
        print(Categors[categoryPicker.selectedRowInComponent(0)].totalTimeList.count)
        timeListTableView.reloadData()
    }
    
    
    
    
}


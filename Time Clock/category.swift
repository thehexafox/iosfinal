//
//  Category.swift
//  Time Clock
//
//  Created by Jeffrey Farnworth on 4/22/16.
//  Copyright © 2016 Jeffrey Farnworth. All rights reserved.
//

import Foundation
import RealmSwift

//This class is created when a user creates a new category to track
class Category {
        dynamic var categoryName = ""
        dynamic var totalTotalTime = 0.0
        dynamic var startTime = NSTimeInterval()
        //Unfortunately, inheriting from Object doesn't allow you to create arrays, so this isn't currently compatible with Realm
        dynamic var totalTimeList = [Double]()
        dynamic var totalTimeListString = [String]()
        dynamic var timeDates = [NSDate]()
        dynamic var timeDatesString = [String]()
        dynamic var thisInstanceTime = 0.0
}
